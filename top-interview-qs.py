
class Solution(object):
    def reverseString(self, s):

		"""
		Question 1

		Write a function that reverses a string. The input string is given as an array of characters char[].

		Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.

		Example 1:

		Input: ["h","e","l","l","o"]
		Output: ["o","l","l","e","h"]

		"""

        """
        :type s: List[str]
        :rtype: None Do not return anything, modify s in-place instead.
        """
        start = 0
        end = len(s)-1
        
        while end>=start:
            temp = s[start]
            s[start]=s[end]
            s[end] = temp
            start= start + 1
            end = end-1
        return s